#include <linux/limits.h>
#include <stdio.h>

int main() {

  char path[PATH_MAX];

  FILE *fp = popen("python3 -c 'print(\"goodbye\")'", "r");

  if (fp == NULL)
    perror("Error opening file");

  fputs("print('goodbye')", fp);
  // fputs("quit()", fp);

  while (fgets(path, PATH_MAX, fp) != NULL)
    printf("%s", path);

  pclose(fp);
}
