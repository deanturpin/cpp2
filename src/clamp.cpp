#include <algorithm>
#include <cassert>

int main() {

  const int a = std::clamp(-1, 0, 10);
  const int b = std::clamp(100, 0, 10);
  const int c = std::clamp(5, 0, 10);

  assert(a == 0);
  assert(b == 10);
  assert(c == 5);
}
