#include <iostream>

struct S {

  int i = 4;
  int j = 5;
};

int main() { [[maybe_unused]] const auto [i, j] = S(); }
