#include <iostream>

int main() {

  struct S {
    int a;
    int b;
    int c;
  };

  S s = {.a = 1, .b = 2, .c = 3};

  std::cout << s.a << "\n";
}
